## Effective Contact Diameter

During indentation with the thermal probe, the contact area changes as a function of the indentation depth. In the literature two models can be found. For small indentatation depth $`h`$, the effective contact diameter (which is the diameter of a hypothetical circle, that has the same area as the tip-sample contact interface) has been calculated as the area of a spherical cap with heigth $`h`$:

```math
d_{eff}(h) = 2\sqrt{d_0h}.
```

For a large indentation depth, $`d_{eff}(h)`$ has been calculated as the diameter of a circle with the same surface area as a cone with height $`h`$:

```math
d_{eff}(h) = 2\frac{\sqrt{\sin(\theta/2)}}{\cos(\theta/2)}h
```
While this model is valid for large indents, the error is between 50-75 \% within the first 25 nm. 

As a consequence, a better model for the effective diameter for a spherical end-capped cone is proposed. One solution would be to create a function that that returns the spherical cap effective diameter for small indentation depth and the cone effective diameter for a large indentation detph, by merging the two functions mentioned above. However, this solution cannot be described in one single analytical function.
It can be shown that a good approximation is obtained by using the effective contact area of a truncated cone instead.

 ```math
d_{eff}(h) = 2\frac{\sqrt{\sin(\theta/2)}}{\cos(\theta/2)}\sqrt{(h+h_0)^2-h_0^2}
```

where $`h_0 = \frac{d_{0}}{2}\left(\frac{1}{\sin(\theta/2)}-1\right)`$ [wikipedia:Nose_cone_design] is the distance between the lower end of a perfect cone and a truncated cone.
It can be shown that in first order for small indentation depths $`h \ll d_0`$,
```math
d_{eff}(h) \propto \sqrt{h}
```
which is in agreement with the spherical cap solution for small indentation depths.

The above equation can be simplified to

```math
d_{eff}(h) = 2\frac{\sqrt{\sin(\theta/2)}}{\cos(\theta/2)} \sqrt{ h^2 \left[1+\frac{d_0}{h}\left(\frac{1}{\sin(\theta/2)}-1\right)\right] }.
```

The figure below shows the different effective tip diameters as a function of the indentation depth (*left*) and the corresponding absolute and effectiv errors (*right*). 

![Effective tip diameter](img/effective-tip-diameter.png)**Figure:** *(left) Effective tip diameter for a truncated cone with an opening angle of 30°, a sphere and a cone as a function of indentation depth. (right) Absolute and relative error of the effective diameter of the sphere and the cone with respect to the truncated cone model.*